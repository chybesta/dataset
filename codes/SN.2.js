const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');

const tags = require('./tags');
const url = 'https://google.github.io/styleguide/htmlcssguide.html';

request(url, (error, response, html) => {
  if (!error && response.statusCode == 200) {
    const $ = cheerio.load(html);
    let arrayOfDataExtracts = [];

    $('#content').each((index, element) => {
      const paragraphtexts = $(element).find('p').text().replace(/\s\s+/g, '');
      const sentences = paragraphtexts.split(".");

      sentences.forEach((sentence) => {
        const arrOfWords = sentence.toLowerCase().split(" ");
        if (arrOfWords.some(word => tags.includes(word))) {
          let object = {};
          object["*"] = arrOfWords.join(" ")
          arrayOfDataExtracts.push(object)
        }
      })
    })

    const data = JSON.stringify(arrayOfDataExtracts);
    fs.writeFileSync(`extracts/SN.${2}.json`, data);
    console.log('Data Extracted Successfully');
  }
});
