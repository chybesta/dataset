const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');

// const tags = require('./tags'); No filtering was done here

const url = 'https://blog.thefirehoseproject.com/posts/100-essential-lessons-for-those-who-want-to-learn-to-code-in-2017';

request(url, (error, response, html) => {
  if (!error && response.statusCode == 200) {
    const $ = cheerio.load(html);
    let arrayOfDataExtracts = [];

    $('.entry-content').each((index, element) => {
      const paragraphtexts = $(element).find('p').text().replace(/\s\s+/g, '');
      const sentences = paragraphtexts.split(".");

      sentences.forEach((sentence, index) => {
        let object = {};
        object["*"] = sentence
        arrayOfDataExtracts.push(object)
      })
    })

    const data = JSON.stringify(arrayOfDataExtracts);
    fs.writeFileSync(`extracts/SN.${4}.json`, data);
    console.log('Data Extracted Successfully');
  }
});
