## Dataset
A collection of data from different sources which is transformed to array of objects stored in json using cheerio and node file system module.

> To install:-
> npm install or yarn install

> 
> To start the server:- 
> npm run start or yarn start
