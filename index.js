const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');

const tags = require('./tags');
const url = 'https://www.tutorialspoint.com/computer_programming/computer_programming_data_types.htm';

request(url, (error, response, html) => {
  if (!error && response.statusCode == 200) {
    const $ = cheerio.load(html);
    let arrayOfDataExtracts = [];
 
    $('.mui-col-md-6.tutorial-content').each((index, element) => {
      const paragraphtexts = $(element).find('p').text();
      const sentences = paragraphtexts.split(". ");

      sentences.forEach((sentence, index) => { 
        const arrOfWords = sentence.toLowerCase().split(" ");
        let numOfTagsInSentence = 0;
        let wordsThatHasBeenChecked = [];

        arrOfWords.forEach(word => {
          if (tags.includes(word) && !wordsThatHasBeenChecked.includes(word)) {
            wordsThatHasBeenChecked.push(word);
            numOfTagsInSentence++;
          }
        })
       
        if (numOfTagsInSentence > 2) {
          if (sentence.length > 10) {
            let object = {};
            object["*"] = sentence.replace(/[^a-zA-Z0-9 ]/g, "").trim()
            arrayOfDataExtracts.push(object)
          }
        }
      })
    })

    console.log(arrayOfDataExtracts.length)
    const data = JSON.stringify(arrayOfDataExtracts);
    fs.writeFileSync(`extracts/filters-by-three-tags/${10}.json`, data);
    console.log('Data Extracted Successfully');
  }
});
